import java.unitl.Date;
public class Movimento{
private Date dataRischiesta;
private date dataValuta;
private String descrizione;
private String tipologia;
private double importo;

public Movimento (Date dataRichiesta, Date dataValuta, String descrizione, String tipologia, double importo){
    this.dataRichiesta = dataRichiesta;
    this.dataValuta = dataValuta;
    this.descrizione = descrizione;
    this.tipologia = tipologia;
    this.importo = importo;
}
public Date getDataRichiesta(){
return dataRichiesta;
}
public Date getDataValuta(){
return dataValuta;
}
public String getDescrizione(){
return descrizione;
}
public String getTipologia(){
return tipologia;
}
public double getImporto(){
return importo;
}
}
